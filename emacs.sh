[[ $TERM == eterm-color ]] && export TERM=xterm

# e path/to/file[:42]
e() {
    echo $1 | \
        awk '{split($0,f,":"); if (f[2]) { print "+" f[2],f[1] } else { print f[1] }}' | \
        xargs emacsclient -n
}

export EDITOR=emacsclient
