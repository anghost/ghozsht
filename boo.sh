# You source this and then it sources everything else

autoload -U compinit; compinit

GHOZSHTDIR=$(dirname "$0")

INCLUDES=("prompt" "emacs" "ls" "history")

for INCLUDE in "${INCLUDES[@]}"; do
    source "$GHOZSHTDIR/$INCLUDE.sh"
done
