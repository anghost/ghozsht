autoload -U colors; colors

ghozsht_prompt_cms_dir() {
    if [ -d .cms ]; then
        if [ $INSIDE_EMACS ]; then
            echo ' (◟ᅇ)◜'
        else
            echo ' 👻'
        fi
    fi
}

ghozhst_prompt_git_branch() {
    BRANCH=$(git symbolic-ref --short HEAD 2> /dev/null)
    if [ $BRANCH ]; then
        echo "%{$fg[cyan]%} $BRANCH%{$reset_color%}"
    fi
}

setopt prompt_subst
PROMPT_1='%{$terminfo[bold]$fg[green]%}%~%{$reset_color%}%{$reset_color%}$(ghozhst_prompt_git_branch)$(ghozsht_prompt_cms_dir)'
PROMPT_2='%{$terminfo[bold]$fg[yellow]%}%#%{$reset_color%} '
RPROMPT='%{$fg[yellow]%}%?%{$reset_color%}'
if [[ -n "$INSIDE_EMACS" ]]; then
    PROMPT_1="$PROMPT_1 $RPROMPT"
    RPROMPT=""
fi
PROMPT="$PROMPT_1
$PROMPT_2"
