# GHOZSHT

Haunting my shell (in a good way).

## Working it

Clone the repo, then source `boo.sh` in `.zshrc`.

## History

I think I used to do this backwards, trying to make my `.zshrc` the
thing in version control. But install scripts tend to want to mess
with it, so let them. I'll share my configurations in something other
things won't try to touch.
